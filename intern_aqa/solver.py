class Solver:
    def __init__(self, a, b) -> None:
        self.a = a
        self.b = b

    def add(self):
        """Функция возвращает сумму двух чисел."""
        return self.a + self.b

    def multyply(self):
        """Функция возвращает произведение двух чисел."""
        return self.a * self.b

    def square_num(*a: int):
        """Функция возвращает список квадратов чисел от 0 до a-1."""
        return [i**2 for i in list(a)]
