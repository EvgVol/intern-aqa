from intern_aqa.solver import Solver
from pytest import mark, param


class TestMain:
    def test_add(self, solver_multi: Solver):
        result = solver_multi.add()
        assert result == solver_multi.a + solver_multi.b

    def test_multipi_one(self, solver: Solver):
        result = solver.multyply()
        assert result == 6

    @mark.parametrize(
        "a, b, expected",
        [
            (2, 3, 6),
            (3, 3, 9),
            (12, 2, 24),
            param(5, 5, 25, id="5 * 5"),
            param(0, 0, 0, id="zero!"),
        ],
    )
    def test_multypli(self, a, b, expected):
        solver = Solver(a=a, b=b)
        result = solver.multyply()
        assert result == expected

    @mark.parametrize(
        "solver_multi_mix, expected",
        [
            param((3, 4), 12, id="3 * 4 = 12"),
            param((4, 4), 16, id="4 * 4 = 16"),
            param((4, 0), 0, id="4 * 0 = 0"),
        ],
        indirect=["solver_multi_mix"],
    )
    def test_multypli_mix(self, solver_multi_mix: Solver, expected: int):
        result = solver_multi_mix.multyply()
        assert result == expected
