import pytest
from intern_aqa.solver import Solver


@pytest.fixture
def a():
    num = 2
    yield num


@pytest.fixture
def b():
    num = 3
    yield num


@pytest.fixture
def solver():
    return Solver(2, 3)


@pytest.fixture(
    params=[
        pytest.param((2, 3), id="first"),
        pytest.param((0, 0), id="second"),
        pytest.param((4, 5), id="three"),
    ]
)
def solver_multi(request):
    a, b = request.param
    solver = Solver(a, b)
    return solver


@pytest.fixture
def solver_multi_mix(request):
    a, b = request.param
    solver = Solver(a, b)
    yield solver
    print("closed")
